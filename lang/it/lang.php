<?php return [
    'plugin' => [
        'name' => 'Profile',
        'description' => 'Quivi Profile Management'
    ],
    'menu' => [
        'main' => 'Utenti',
        'side' => [
            'profiles' => 'Profili',
            'types' => 'Tipologie Profili',
            'users' => 'Utenti',
            'techniques' => 'Lavorazioni',
            'techniquestypes' => 'Tipologie Lavorazioni',
        ],
    ],
    'profile' => [
        'profile' => 'Profilo',
        'profiles' => 'Profili',
        'create' => 'Crea nuovo Profilo',
        'update' => 'Modifica Profilo',
        'preview' => 'Anteprima Profilo',
        'type' => 'Tipologia di Profilo',
        'user' => 'Profilo associato all\'Utente',
        'ceo' => 'Amministratore della Società/Associazione',
    ],
    'techniquetype' => [
        'techniquetype' => 'Tipologia Lavorazione',
        'techniquestypes' => 'Tipologie Lavorazione',
        'create' => 'Crea nuova Tipologia Lavorazione',
        'update' => 'Modifica Tipologia Lavorazione',
        'preview' => 'Anteprima Tipologia Lavorazione'
    ]
];