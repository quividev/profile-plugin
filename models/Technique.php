<?php namespace Quivi\Profile\Models;

use Model;

/**
 * Model
 */
class Technique extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'quivi_profile_techniques';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsToMany = [
        'products' => [
            'Quivi\Product\Models\Product',
            'table' => 'quivi_techniques_products_techniques',
            'pivot' => ['notes'],
            'timestamps' => true,
            'pivotModel' => 'Quivi\Profile\Models\ProductTechniquePivot',
        ],
        'profiles' => [
            'Quivi\Profile\Models\Profile',
            'table' => 'quivi_profiles_profiles_techniques',
            'pivot' => ['is_priority', 'notes'],
            'timestamps' => true,
            'pivotModel' => 'Quivi\Profile\Models\ProfileTechniquePivot',
        ],
    ];

    public $belongsTo = [
        'technique_type' => 'Quivi\Profile\Models\TechniqueType',
    ];

}
