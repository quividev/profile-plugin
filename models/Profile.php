<?php namespace Quivi\Profile\Models;

use Model;

/**
 * Model
 */
class Profile extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'quivi_profile_profiles';

    public $implement = ['RainLab.Location.Behaviors.LocationModel'];

    public $belongsTo = [
        'type' => [
            'Quivi\Profile\Models\Type',
            'order' => 'name asc'
        ],
        'user' => 'Rainlab\User\Models\User',
    ];

    public $belongsToMany = [
        'groups' => [
            'Rainlab\User\Models\UserGroup', 
            'table' => 'quivi_profile_profiles_groups',
            'key' => 'profile_id',
            'otherKey' => 'user_group_id'
        ],
        'techniques_count' => [
            'Quivi\Profile\Models\Technique',
            'table' => 'quivi_profile_profiles_techniques',
            'count' => true
        ],
        'techniques_pivot_model' => [
            'Quivi\Profile\Models\Technique',
            'table' => 'quivi_profile_profiles_techniques',
            'pivot' => ['is_priority', 'notes'],
            'timestamps' => true,
            'pivotModel' => 'Quivi\Profile\Models\ProfileTechniquePivot',
        ]
    ];

    public $hasOne = [
        'ceo' => ['Quivi\Profile\Models\Profile', 'key' => 'ceo_id'],
    ];

    public $hasMany = [
        'users' => 'Rainlab\User\Models\User',
    ];

    protected $legacy = 
    [
        'customer_sid' => 'sid',
        'customer_ceo_id' => 'ceo_id',
        'customer_firstname' => 'firstname',
        'customer_lastname' => 'lastname',
        'customer_ragsoc' => 'company_name',
        'customer_address' => 'address',
        'customer_zip' => 'zip',
        'customer_prov' => 'prov',
        'customer_city' => 'city',
        //TODO: update with plugin location data # 'customer_country_id' => 'country_id',
        'customer_phone' => 'phone',
        'customer_fax' => 'fax',
        'customer_piva' => 'vat',
        'customer_codfis' => 'cf',
        'customer_email' => 'email',
        'customer_ip_reg' => 'ip_reg',
        'customer_created_at' => 'created_at',
        'customer_updated_at' => 'updated_at',
    ];


    public function beforeCreate()
    {
        if (!$this->sid){
            $this->sid = md5(time().'quivi.profile');
        }
    }

    public function beforeSave()
    {
    }

    public function ingest($object){

        foreach ($this->legacy as $old => $new){
            if ($new && isset($object->$old) && ($object->$old != '0000-00-00 00:00:00')) {
                $this->$new = $object->$old;
            }
        }

        switch ($object->customer_type){
            case 'PF': 
                $this->type_id = 1;
                break;
            case 'PRO':
                $this->type_id = 2;
                break;
            default:
                $this->type_id = 3;
                break;
        }

        $this->privacy_at = $this->terms_at = $this->created_at;
    }

    public function scopeCustomer($query) {
        //TODO
        //$query->with('groups')->whereHas('groups', function($q){
        //    $q->where('code','designer');
        //});
    }

    public function scopeCompany($query) {

        $query->with('type')->whereHas('type', function($q){
            $q->where('code','AZ');
        });

    }

    public function getCompleteName(){
        if ($this->company_name) return $this->company_name;
        elseif ($this->lastname) return $this->firstname. " ".$this->lastname;
    }
}
