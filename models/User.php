<?php

namespace Quivi\Profile\Models;

use Exception;
use Illuminate\Support\Collection;
use RainLab\User\Models\User as UserBase;

class User extends UserBase
{
    public $hasMany = [
        'profiles' => 'Quivi\Profile\Models\Profile',
    ];

    public $hasOne = [
        'profile' => 'Quivi\Profile\Models\Profile',
    ];

}