<?php namespace Quivi\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQuiviProfileProfilesTechniques extends Migration
{
    public function up()
    {
        Schema::create('quivi_profile_profiles_techniques', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('profile_id')->unsigned();
            $table->integer('technique_id')->unsigned();
            $table->string('notes')->nullable();
            $table->string('is_priority')->boolean()->default(false);
            $table->timestamps();
            $table->primary(['profile_id', 'technique_id'], 'profile_technique');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('quivi_profile_profiles_techniques');
    }
}
