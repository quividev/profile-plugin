<?php namespace Quivi\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQuiviProfileProfilesGroup extends Migration
{
    public function up()
    {
        Schema::create('quivi_profile_profiles_groups', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('profile_id')->unsigned();
            $table->integer('user_group_id')->unsigned();
            $table->primary(['profile_id', 'user_group_id'], 'profile_group');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('quivi_profile_profiles_groups');
    }
}
