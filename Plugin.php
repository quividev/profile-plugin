<?php namespace Quivi\Profile;

use System\Classes\PluginBase;


use App;
use Config;
use Rainlab\User\Controllers\Users as UsersController;
use Rainlab\User\Models\User as UserModel;

use Auth;
use Event;
use Mail;
use Input;
use Backend;

class Plugin extends PluginBase
{
    public $require = ['RainLab.User'];

    public function registerComponents()
    {
        return [
        ];
    }

    public function registerSettings()
    {
    }
    
    public function register()
    {
    }

    public function registerPermissions()
    {
        return [
            'quivi.profile.manage_settings' => [
                'tab'   => 'quivi.profile::lang.plugin.name',
                'label' => 'quivi.profile::lang.plugin.manage_settings_permission',
            ],
        ];
    }
    
    public function boot(){

        Event::listen('backend.menu.extendItems', function($manager) {            

            // Add new side menu items 
            // parameters: Plugin ID, Menu Code, Menu Definitions. 
            $manager->addSideMenuItems('RainLab.User', 'user', [
                'users' => [
                    'label' => 'quivi.profile::lang.menu.side.users',
                    'icon'  => 'icon-user',
                    'code'  => 'users',
                    'owner' => 'RainLab.User',
                    'url'   => Backend::url('rainlab/user/users')
                ],
                'profiles' => [
                    'label' => 'quivi.profile::lang.menu.side.profiles',
                    'icon'  => 'icon-users',
                    'code'  => 'profiles',
                    'owner' => 'RainLab.User',
                    'url'   => Backend::url('quivi/profile/profiles')
                ],
                'types' => [
                    'label' => 'quivi.profile::lang.menu.side.types',
                    'icon'  => 'icon-tags',
                    'code'  => 'types',
                    'owner' => 'RainLab.User',
                    'url'   => Backend::url('quivi/profile/types')
                ],
                'techniques' => [
                    'label' => 'quivi.profile::lang.menu.side.techniques',
                    'icon'  => 'icon-wrench',
                    'code'  => 'techniques',
                    'owner' => 'RainLab.User',
                    'url'   => Backend::url('quivi/profile/techniques')
                ],
                'techniquestypes' => [
                    'label' => 'quivi.profile::lang.techniquetype.techniquestypes',
                    'icon'  => 'icon-folder',
                    'code'  => 'techniquestypes',
                    'owner' => 'RainLab.User',
                    'url'   => Backend::url('quivi/profile/techniquestypes')
                ],
            ]);

            Event::fire('backend.menu.extendItems.profile', [$manager]);
    
        });

        Event::listen('rainlab.user.activate', function($user) {
            
            /*
            // User Notification
            Mail::send('quivi.profile::activation.notify', [
                'name' => $user->name,
                'surname' => $user->surname,
                'email' => $user->email
            ], function($message) {
                foreach (explode(",", env('ADMIN_EMAIL')) as $admin_email){
                    $message->to($admin_email);    
                }
                $message->subject('Conferma Attivazione Nuovo Utente');
            });
            */

        });

        UserModel::extend(function($model){
            $model->belongsTo['profile'] = ['Quivi\Profile\Models\Profile', 'key' => 'profile_id', 'otherKey' => 'id'];
            $model->hasMany['profiles'] = ['Quivi\Profile\Models\Profile', 'key' => 'id', 'otherKey' => 'user_id'];            
        });

        UsersController::extendFormFields(function($form, $model, $context){

            $form->addTabFields([
                'profile' => [
                    'label' => 'quivi.profile::lang.profile.profile',
                    'type' => 'relation',
                    'nameFrom' => "CONCAT(surname, ' ', name)",
                    'tab' => 'rainlab.user::lang.user.account'
                ],
                
            ]);

        });

    }
}
